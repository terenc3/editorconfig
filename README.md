# editorconfig
> Global editorconfig file with sensible defaults.

## Files
* apib
* bat,cmd
* java
* json
* md
* php
* py
* sh
* yaml

## Programms
* bower
* editorConfig checker
* git
* make
* npm
* sublime
* tortoisegit
* vagrant

# Usage
Clone repository and link editorconfig file to ~/.editorconfig.

```
git clone git@codeberg.org:terenc3/editorconfig.git
ln editorconfig/editorconfig.ini ~/.editorconfig
```
